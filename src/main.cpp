
#include <Arduino.h>
#include "Temp.h"


const int sensorPin = A0;

void setup() {
  Serial.begin(115200);
}

void loop() {
  int s = Serial.read();
  float voltage = (analogRead(sensorPin) / 1024.0 ) * 5.0;
  Temperature t{voltage};
  switch (s) {
    case Units::celsius:
      Serial.println(t.celsius());
      break;
    case Units::fahrenheit:
      Serial.println(t.fahrenheit());
      break;
    default:
      // TODO what to do if bad request?
      break;
  }
}
