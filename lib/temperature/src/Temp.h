#ifndef TEMP_H_
#define TEMP_H_

enum Units {celsius = 0x63, fahrenheit = 0x66};

class Temperature
{
public:

  Temperature(float voltage):
    voltage_(voltage) {}

  float celsius();
  float fahrenheit();

private:
  float voltage_;
};

#endif // TEMP_H_
