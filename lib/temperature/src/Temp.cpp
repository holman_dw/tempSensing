#include "Temp.h"

float Temperature::celsius() {
  return (voltage_ - .5) * 100;
}

float Temperature::fahrenheit() {
  return (((voltage_ -.5) * 100) * 1.8) + 32;
}
